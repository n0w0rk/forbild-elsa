clean:
	rm data/abdomen/data.pha
	rm data/head/data.pha
	rm data/thorax/data.pha

preprocess:
	make -C data/

parse:
	if [ -d "../elsa/generators/" ]; then python3 parse.py > ../elsa/generators/ForbildData.h; else echo "Not subdir of elsa project"; fi
