# Getting started
1. clone this project inside your [elsa repo](https://gitlab.lrz.de/IP/elsa) with something like `git clone https://gitlab.lrz.de/ge35qim/forbild-elsa.git`
2. step inside `forbild-elsa` folder
3. run `make parse` to overwrite `../elsa/generators/ForbildData.h`
4. compile the elsa runnable with the new data

If you want to change the `pha` definition do so and run
1. `make preprocess`- which uses the proprocessor to handle the includes ... Historically grown, but this is the FORBILD definition :clock: :shrug:
2. `make parse` to run the parser again

# New phantom
By now `head` `abdomen` and `thorax` are implemented. To add a new phantom you have to:

1. add the subfolder and insert data
2. modify the make file
3. modify `parser.py`
4. `make parse`
5. Have a look at `../elsa/generators/Phantoms.cpp` and `../elsa/generators/ForbildPhantom.cpp` to implement the new code to handle the new phantom

# Background
This is a repository for my bachelor thesis, containing all FORBILD specifications.
Because the website might goes offline, this is a backup.

See [data/README.md](data/README.md) for more details.

# Links
https://gitlab.lrz.de/IP/elsa

http://ftp.imp.uni-erlangen.de/phantoms/

