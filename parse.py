import re
import numpy as np
from scipy.spatial.transform import Rotation
from scipy.spatial import distance as dist
import sys


# def rotation_angles(matrix, order):
#    """
#    @see https://programming-surgeon.com/en/euler-angle-python-en/
#    input
#        matrix = 3x3 rotation matrix (np array)
#        oreder(str) = rotation order of x, y, z : e.g, rotation XZY -- 'xzy'
#    output
#        phi theta psi
#    """
#    r11, r12, r13 = matrix[0]
#    r21, r22, r23 = matrix[1]
#    r31, r32, r33 = matrix[2]
#
#
#    phi = 0
#    theta = 0
#    psi = 0
#
#    if order == 'zxz':
#        theta = np.arctan(np.sqrt(1-r33**2)/r33)
#        if np.sin(theta) == 0:
#            #theta =0
#            phi = np.arccos(r11)
#            psi = 0
#        elif r23 == 0:
#            phi = np.pi /2
#            psi = np.arccos(r21)
#        else:
#          phi = np.arctan(-r13 / r23)
#          psi = np.arctan(r31 / r32)
#
#
#    phi = phi * 180 / np.pi
#    theta = theta * 180 / np.pi
#    psi = psi * 180 / np.pi
#
#    return (phi,theta,psi)


def debugToStdErr(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def search(text, search, val):
    res = re.search(search, text)
    if res:
        return res.groups()[0]
    else:
        return val


def convert(text):
    if text == "":
        return 0
    text = re.sub("cos\(((\d+\.?\d*)|(\.\d+))\)",
                  r"np.cos(np.deg2rad(\1))", text)
    text = re.sub("sin\(((\d+\.?\d*)|(\.\d+))\)",
                  r"np.sin(np.deg2rad(\1))", text)
    text = re.sub("sqrt\(((\d+\.?\d*)|(\.\d+))\)",
                  r"np.sqrt(\1)", text)
    return eval(text)


def evalStr2Str(text):
    return str(convert(text))


def det(xRot, yRot, zRot):
    return np.round(np.linalg.det(np.array([xRot, yRot, zRot]).transpose()), decimals=8)


def vec2EulerAngles(xRot, yRot, zRot):
    """
    Given the 3 vectors of the rotated coordinate system
    this function evaluates the 3 euler angles for the rotation
    """
    mode = ""

    # Searching for the 3rd axis to get a rotation matrix (det has to be 1)
    if xRot.size > 1 and yRot.size > 1:
        xRot = xRot / (np.linalg.norm(xRot) + 1e-16)
        yRot = yRot / (np.linalg.norm(yRot) + 1e-16)
        mode = "zRot"
        zRotCross1 = np.cross(yRot, xRot)
        zRotCross2 = np.cross(xRot, yRot)

        if det(xRot, yRot, zRotCross1) == 1:
            zRot = zRotCross1
        elif det(xRot, yRot, zRotCross2) == 1:
            zRot = zRotCross2
        else:
            debugToStdErr(
                "Error could not find the 3rd axis for a determinant=1")
    elif xRot.size > 1 and zRot.size > 1:
        xRot = xRot / (np.linalg.norm(xRot) + 1e-16)
        zRot = zRot / (np.linalg.norm(zRot) + 1e-16)
        mode = "yRot"
        yRotCross1 = np.cross(zRot, xRot)
        yRotCross2 = np.cross(xRot, zRot)
        if det(xRot, yRotCross1, zRot) == 1:
            yRot = yRotCross1
        elif det(xRot, yRotCross2, zRot) == 1:
            yRot = yRotCross2
        else:
            debugToStdErr(
                "Error could not find the 3rd axis for a determinant=1")
            debugToStdErr("Det:", det(xRot, yRotCross1, zRot),
                          "für", yRotCross1)
            debugToStdErr("Det2:", det(xRot, yRotCross2, zRot),
                          "für", yRotCross2)
    else:
        yRot = yRot / (np.linalg.norm(yRot) + 1e-16)
        zRot = zRot / (np.linalg.norm(zRot) + 1e-16)
        mode = "xRot"
        xRotCross1 = np.cross(zRot, yRot)
        xRotCross2 = np.cross(yRot, zRot)
        if det(xRotCross1, yRot, zRot) == 1:
            xRot = xRotCross1
        elif det(xRotCross2, yRot, zRot) == 1:
            xRot = xRotCross2
        else:
            debugToStdErr(
                "Error could not find the 3rd axis for a determinant=1")

    rot = np.array([xRot, yRot, zRot])
    rot = rot.transpose()
    debugToStdErr(rot)
    r = Rotation.from_matrix(rot)
    debugToStdErr(r.as_matrix())

    angles = r.as_euler("zxz", degrees=True)
    angles = np.round(angles, decimals=4)
    angles = [str(i) for i in angles]
    debugToStdErr("Euler angles", angles)
    [psi, theta, phi] = angles
    return [phi, theta, psi]


def ell(text, dens, order):
    x = y = z = "0"
    dx = dy = dz = "0"

    x = evalStr2Str(search(text, " x=\s*([^\s]*)", x))
    y = evalStr2Str(search(text, " y=\s*([^\s]*)", y))
    z = evalStr2Str(search(text, " z=\s*([^\s]*)", z))
    dx = evalStr2Str(search(text, " dx=\s*([^\s]*)", dx))
    dy = evalStr2Str(search(text, " dy=\s*([^\s]*)", dy))
    dz = evalStr2Str(search(text, " dz=\s*([^\s]*)", dz))

    return "{{ {},{},{},0.0,0.0,0.0,{} }}".format(dens,  ",".join([dx, dy, dz]), ",".join([x, y, z]), order)


def ellClippingX(text, dens, order):
    x = y = z = "0"
    dx = dy = dz = "0"
    clippingX = "std::numeric_limits<double>::infinity()"

    x = evalStr2Str(search(text, " x=\s*([^\s]*)", x))
    y = evalStr2Str(search(text, " y=\s*([^\s]*)", y))
    z = evalStr2Str(search(text, " z=\s*([^\s]*)", z))
    dx = evalStr2Str(search(text, " dx=\s*([^\s]*)", dx))
    dy = evalStr2Str(search(text, " dy=\s*([^\s]*)", dy))
    dz = evalStr2Str(search(text, " dz=\s*([^\s]*)", dz))

    clippingX = search(text, " x<\s*([^\s]*)", clippingX)
    return "{{ {},{},{},0.0,0.0,0.0,{},{} }}".format(dens,  ",".join([dx, dy, dz]), ",".join([x, y, z]), clippingX, order)


def ellFree(text, dens, order):
    x = y = z = "0"
    dx = dy = dz = "0"
    ax = ay = az = ""

    x = evalStr2Str(search(text, " x=\s*([^\s]*)", x))
    y = evalStr2Str(search(text, " y=\s*([^\s]*)", y))
    z = evalStr2Str(search(text, " z=\s*([^\s]*)", z))
    dx = evalStr2Str(search(text, " dx=\s*([^\s]*)", dx))
    dy = evalStr2Str(search(text, " dy=\s*([^\s]*)", dy))
    dz = evalStr2Str(search(text, " dz=\s*([^\s]*)", dz))

    ax = search(text, "a_x\(([^a|^\]]*)\) ", ax)
    ay = search(text, "a_y\(([^a|^\]]*)\) ", ay)
    az = search(text, "a_z\(([^a|^\]]*)\) ", az)

    xRot = np.array([convert(i) for i in ax.split(",")])
    yRot = np.array([convert(i) for i in ay.split(",")])
    zRot = np.array([convert(i) for i in az.split(",")])

    return "{{ {},{},{},{},{} }}".format(dens,  ",".join([dx, dy, dz]), ",".join([x, y, z]), ",".join(vec2EulerAngles(xRot, yRot, zRot)), order)


def sphere(text, dens, order):
    x = y = z = "0"
    r = "0"

    x = evalStr2Str(search(text, " x=\s*([^\s]*)", x))
    y = evalStr2Str(search(text, " y=\s*([^\s]*)", y))
    z = evalStr2Str(search(text, " z=\s*([^\s]*)", z))

    r = search(text, " r=\s*([^\s]*)", r)

    return "{{ {},{},{},{} }}".format(dens,  ",".join([x, y, z]), r, order)


def ellCyl(text, dens, order):
    orientation = ""

    x = y = z = "0"
    dx = dy = dz = "0"
    length = "0"

    x = evalStr2Str(search(text, " x=\s*([^\s]*)", x))
    y = evalStr2Str(search(text, " y=\s*([^\s]*)", y))
    z = evalStr2Str(search(text, " z=\s*([^\s]*)", z))
    elDimensions = []
    if "Cyl_x" in text:
        orientation = "0"
        dy = search(text, " dy=\s*([^\s]*)", dy)
        dz = search(text, " dz=\s*([^\s]*)", dz)
        elDimensions = [dy, dz]
    elif "Cyl_y" in text:
        orientation = "1"
        dx = search(text, " dx=\s*([^\s]*)", dx)
        dz = search(text, " dz=\s*([^\s]*)", dz)
        elDimensions = [dx, dz]

    elif "Cyl_z" in text:
        orientation = "2"
        dx = search(text, " dx=\s*([^\s]*)", dx)
        dy = search(text, " dy=\s*([^\s]*)", dy)
        elDimensions = [dx, dy]

    length = search(text, "l=([^\s]+) ", length)

    return "{{ {},{},{},{},{},{} }}".format(orientation, dens, length,  ",".join([x, y, z]), ",".join(elDimensions), order)


def ellCylsFree(text, dens, order):

    x = y = z = "0"
    dx = dy = "0"
    ax = ay = az = ""
    length = "0"

    x = evalStr2Str(search(text, " x=\s*([^\s]*)", x))
    y = evalStr2Str(search(text, " y=\s*([^\s]*)", y))
    z = evalStr2Str(search(text, " z=\s*([^\s]*)", z))

    dx = evalStr2Str(search(text, " dx=\s*([^\s]*)", dx))
    dy = evalStr2Str(search(text, " dy=\s*([^\s]*)", dy))

    ax = search(text, "a_x\(([^a|^\]]*)\) ", ax)
    ay = search(text, "a_y\(([^a|^\]]*)\) ", ay)
    az = search(text, "(?:axis|achse)\(([^a|^\]]*)\) ", az)

    length = evalStr2Str(search(text, "l=([^\s]+) ", length))

    xRot = np.array([convert(i) for i in ax.split(",")])
    yRot = np.array([convert(i) for i in ay.split(",")])
    zRot = np.array([convert(i) for i in az.split(",")])

    eulerangles = vec2EulerAngles(xRot, yRot, zRot)

    return "{{ {},{},{},{},{},{} }}".format(dens, length,  ",".join([x, y, z]),  ",".join([dx, dy]), ",".join(eulerangles), order)


def cyl(text, dens, order):
    orientation = ""

    x = y = z = "0"
    radius = "0"
    length = "0"

    x = evalStr2Str(search(text, " x=\s*([^\s]*)", x))
    y = evalStr2Str(search(text, " y=\s*([^\s]*)", y))
    z = evalStr2Str(search(text, " z=\s*([^\s]*)", z))

    if "Cylinder_x" in text:
        orientation = "0"
    elif "Cylinder_y" in text:
        orientation = "1"

    elif "Cylinder_z" in text:
        orientation = "2"

    length = search(text, "l=([^\s]+) ", length)
    radius = search(text, "r=([^\s]+) ", radius)

    return "{{ {},{},{},{},{},{} }}".format(orientation, dens, ",".join([x, y, z]), radius, length, order)


def rotation_matrix_from_vectors(vec1, vec2):
    """ Find the rotation matrix that aligns vec1 to vec2
    :param vec1: A 3d "source" vector
    :param vec2: A 3d "destination" vector
    :return mat: A transform matrix (3x3) which when applied to vec1, aligns it with vec2.

    @see https://stackoverflow.com/questions/45142959/calculate-rotation-matrix-to-align-two-vectors-in-3d-space
    """
    a, b = (vec1 / np.linalg.norm(vec1)).reshape(3), (vec2 /
                                                      np.linalg.norm(vec2)).reshape(3)
    v = np.cross(a, b)
    c = np.dot(a, b)
    s = np.linalg.norm(v)
    kmat = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    rotation_matrix = np.eye(3) + kmat + kmat.dot(kmat) * ((1 - c) / (s ** 2))
    return rotation_matrix


def cylFree(text, dens, order):
    x = y = z = "0"
    axis = ""
    radius = "0"
    length = "0"

    x = evalStr2Str(search(text, " x=\s*([^\s]*)", x))
    y = evalStr2Str(search(text, " y=\s*([^\s]*)", y))
    z = evalStr2Str(search(text, " z=\s*([^\s]*)", z))

    axis = search(text, "axis\s*\(([^a|^\]]*)\) ", axis)

    length = search(text, "l=([^\s]+) ", length)
    radius = search(text, "r=([^\s]+) ", radius)

    vec = np.array([convert(i) for i in axis.split(",")])
    r = Rotation.from_matrix(
        rotation_matrix_from_vectors(np.array([0, 0, 1]), vec))
    angles = r.as_euler("zxz", degrees=True)
    angles = np.round(angles, decimals=4)
    angles = [str(i) for i in angles]
    debugToStdErr("Vec", axis)
    debugToStdErr("VecNorm", vec)
    debugToStdErr("Rotationmatrix", r.as_matrix(), "\n")
    debugToStdErr("euler cllyp", angles)

    [psi, theta, phi] = angles
    return "{{ {},{},{},{},{},{} }}".format(dens,  ",".join([x, y, z]), radius, length, ",".join([phi, theta, psi]), order)


def cone(text, dens, order):
    orientation = ""

    x = y = z = "0"
    radius1 = "0"
    radius2 = "0"
    length = "0"

    x = evalStr2Str(search(text, " x=\s*([^\s]*)", x))
    y = evalStr2Str(search(text, " y=\s*([^\s]*)", y))
    z = evalStr2Str(search(text, " z=\s*([^\s]*)", z))

    if "Cone_x" in text:
        orientation = "0"
    elif "Cone_y" in text:
        orientation = "1"
    elif "Cone_z" in text:
        orientation = "2"

    length = search(text, "l=([^\s]+) ", length)
    radius1 = search(text, "r1=([^\s]+) ", radius1)
    radius2 = search(text, "r2=([^\s]+) ", radius2)

    return "{{ {},{},{},{},{},{},{} }}".format(orientation, dens, ",".join([x, y, z]), radius1, radius2, length, order)

def box(text, dens, order):
    x = y = z = "0"
    dx = dy = dz = "0"

    x = evalStr2Str(search(text, " x=\s*([^\s]*)", x))
    y = evalStr2Str(search(text, " y=\s*([^\s]*)", y))
    z = evalStr2Str(search(text, " z=\s*([^\s]*)", z))

    dx = evalStr2Str(search(text, " dx=\s*([^\s]*)", dx))
    dy = evalStr2Str(search(text, " dy=\s*([^\s]*)", dy))
    dz = evalStr2Str(search(text, " dz=\s*([^\s]*)", dz))

    return "{{ {},{},{},{} }}".format(dens, ",".join([x, y, z]), ",".join([dx, dy, dz]), order)


def eulerAngleFromRotationMatrix(matrix):
    """
    @see http://eecs.qmul.ac.uk/~gslabaugh/publications/euler.pdf
    code for zxy rotation
    """
    phi = 0
    theta = 0
    psi = 0
    if abs(matrix[2][0]) != 1:
        phi = -np.arcsin(matrix[2][0])
        theta = np.arctan2(matrix[2][1] / np.cos(phi),
                           matrix[2][2] / np.cos(phi))
        psi = np.arctan2(matrix[1][0] / np.cos(phi),
                         matrix[0][0] / np.cos(phi))
    elif matrix[3][1] == -1:
        phi = 0
        theta = np.pi/2
        psi = np.atan2(matrix[0][1], matrix[0][2])
    else:
        psi = 0
        theta = -np.pi/2
        psi = np.atan2(-matrix[0][1], -matrix[0][2])

    degrees = [str((i*180)/np.pi) for i in [phi, theta, psi]]
    return degrees


def replaceArray(replacements, array):
    out = []
    for text in array:
        for (key, val) in replacements:
            text = text.replace(key, val)
        out.append(text)
    return out


def printArray(prae, varName, array, length):

    if len(array) > 0:
        print("template <typename data_t, typename = std::enable_if_t<std::is_floating_point<data_t>::value>> inline const std::vector<std::array<data_t,"+length+">> "+prae +
              "_" + varName+" {{ ", ",\n".join(array), " }};")
    else:
        print("template <typename data_t, typename = std::enable_if_t<std::is_floating_point<data_t>::value>> inline const std::vector<std::array<data_t,"+length+">> "+prae +
              "_" + varName+" {{ }};")


def parse(path, varName):
    with open(path) as f:
        text = f.read()
        text = text.replace("\n", "")
        ellipsoids = []
        ellipsoidsClippingX = []
        spheres = []
        ellipCyls = []
        ellipCylsFree = []
        cyls = []
        cylsFree = []
        cones = []
        boxes = []
        order = 0
        for i in re.findall("{([^}]*)}", text):
            dens = search(i, "(?:rho|dichte)=(([0-9]*[.])?[0-9]+)", "0")
            name = search(i, "([^\[]*)", "no name")
            i = i.replace("[", " ")
            i = i.replace("]", " ")
            if len(name) > 1:
                debugToStdErr("Parse name:", name)
            if "Ellipsoid_free" in i:
                ellipsoids.append(ellFree(i, dens, order))
                order += 1
            elif "Ellipsoid" in i:
                if "x<" in i:  # Clipping #TODO other kinds of clipping are missing, this needed for head FORBIL Head right ear
                    ellipsoidsClippingX.append(ellClippingX(i, dens, order))
                    order += 1
                else:
                    ellipsoids.append(ell(i, dens, order))
                    order += 1
            elif "Sphere" in i:
                spheres.append(sphere(i, dens, order))
                order += 1
            elif "Ellipt_Cyl_" in i:
                ellipCyls.append(ellCyl(i, dens, order))
                order += 1
            elif "Ellipt_Cyl" in i:
                ellipCylsFree.append(ellCylsFree(i, dens, order))
                order += 1
            elif "Cylinder_" in i:
                cyls.append(cyl(i, dens, order))
                order += 1
            elif "Cylinder" in i:
                cylsFree.append(cylFree(i, dens, order))
                order += 1
            elif "Box" in i:
                boxes.append(box(i, dens, order))
                order += 1
            elif "Cone_" in i:
                cones.append(cone(i, dens, order))
                order += 1

            debugToStdErr("END ############## ", name)

        printArray("ellipsoids", varName, ellipsoids, "11")
        printArray("ellipsoidsClippingX", varName, ellipsoidsClippingX, "12")
        printArray("spheres", varName, spheres, "6")
        printArray("ellipCyls", varName, ellipCyls, "9")
        printArray("ellipCylsFree", varName, ellipCylsFree, "11")
        printArray("cyls", varName, cyls, "8")
        printArray("cylsFree", varName, cylsFree, "10")
        printArray("cones", varName, cones, "9")
        printArray("boxes", varName, boxes, "8")
        print("const int maxOrderIndex_{} = {};".format(varName, order))


print("// clang-format off")
print("""/*
* This is an auto-generated file by the parser from https://gitlab.lrz.de/ge35qim/forbild-elsa.
* The parser provides an array of each primitive shape for the abdomen, head and thorax FORBILD phantom.
* Because the FORBILD phantom has no blending, the rasterization order of the objects is defined also inside
* the arrays. Each object has a `maxOrderIndex_<phantom>` variable which defines the maximal order index.
*/""")
print("#include <limits>")
print("namespace elsa::phantoms::forbilddata{")
print("#pragma GCC diagnostic push")
print("#pragma GCC diagnostic ignored \"-Wfloat-conversion\"")
parse("data/abdomen/data.pha", "abdomen")
parse("data/head/data.pha", "head")
parse("data/thorax/data.pha", "thorax")
print("#pragma GCC diagnostic pop")
print("} // namespace elsa::phantoms::forbilddata")
print("// clang-format on")
